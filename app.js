const http = require('http');
const fs = require('fs');
const format = require('date-format');
const teerling = require('./teerling');

console.log("Hello");

function requestListener(request, response){
    console.log('request received...!');
    fs.appendFile('webserver.log', `request received at ${format.asString('hh:mm:ss',new Date())}\n`, (err) => {
        if (err) {
            console.error("Schrijven naar log is niet gelukt" + err.message);
            throw err;
        }
    })
    //console.log(teerling.werp());
    fs.readFile('index.html', (err, data) => {
        response.write(data.toString().replace('$$$$',teerling.werp()));
        response.end();
    });

}

let server = http.createServer(requestListener);
server.listen(8081);
